from phileas import html5 as h
import sys, os


def columns():
    for i in range(5):
        yield from h.td % f"column {i} content"


def little_test_page():

    print("yield#0")
    yield  from (h.br + h.br + h.br)

    print("yield#1")
    yield  from (h.h4 | ('dsdfs'))

    print("yield#2")
    yield from  (h.p | (h.em | (h.strong | ('aha!', "asdf"))))
    yield "easy"

    yield from h.table | columns()
def main():
    print("Content-type: text/html\n\n")
    #print(test_page())
    print(''.join(little_test_page()))


if __name__ == '__main__':
    main()
