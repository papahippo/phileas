#!/usr/bin/python3
# -*- encoding: utf8 -*-
from .membersPage import MembersPage, h
from .memberHelper import MemberHelper
from phileas.cherryweb import validator

class AdminPage(MembersPage):
    def authenticate(self, *paths, **kw):
        with MemberHelper(self, *paths, **kw) as context_:
            return validator({'Admin': 'istrator'})

    def upperBanner(self,*paths,   **kw):
        return h.h1(id='upperbanner') % (
            f'{self.clubName} {self(en_GB="(supplemental) - Administration zone", nl_NL="(aanvullend) - Administratiezone")}'
        )
    def lowerBanner(self, context_, **kw):
        return h.h1(id='lowerbanner') % self(en_GB="Administration Homepage",
                                              nl_NL="Homepagina van Administratiezone")

_adminPage = AdminPage()
