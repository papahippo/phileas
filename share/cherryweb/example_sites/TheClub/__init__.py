from phileas.cherryweb import validator, sites_served
import cherrypy
import tempfile, os

from .clubIndexPage import _clubIndexPage
from .membersPage import _membersPage
from .adminPage import _adminPage
_clubIndexPage.members_zone = _membersPage
_clubIndexPage.admin_zone = _adminPage

HERE, _ = os.path.split(__file__)

def _clubConfig(session_path=''):
    return {
    '/':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': HERE,
            'tools.sessions.on': True,
            'tools.sessions.storage_class': cherrypy.lib.sessions.FileSession,
            'tools.sessions.storage_path': session_path,
            'tools.sessions.timeout': 10,
            'tools.sessions.locking': 'early'
        },
    '/members_zone': {
        'tools.sessions.on': True,
        'tools.auth_basic.on': True,
        'tools.auth_basic.realm': 'club members zone',
        'tools.auth_basic.checkpassword': _membersPage.authenticate('current'),
        'tools.auth_basic.accept_charset': 'UTF-8',
    },
    '/admin_zone': {
        'tools.sessions.on': True,
        'tools.auth_basic.on': True,
        'tools.auth_basic.realm': 'club admin zone',
        'tools.auth_basic.checkpassword': _adminPage.authenticate('current'),
        'tools.auth_basic.accept_charset': 'UTF-8',
    },
}
sites_served.append (('/TheClub', _clubIndexPage, _clubConfig))