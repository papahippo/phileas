#!/usr/bin/python3
# -*- encoding: utf8 -*-
from entity.club import Member
from phileas.cherryweb.entityHelper import *
from datetime import date
from babel.numbers import format_decimal

class MemberHelper(EntityHelper):
    EntityClass = Member
    _moduleForImport = 'members'

    fieldDisplay = OrderedDict([
# python name       # heading for glossing                              # field entry tip for glossing
('called',          ({'en_GB':'known as',         'nl_NL':'roepnaam'},         {'en_GB': 'known within MEW as...', 'nl_NL': 'bekend binnen MEW als...'})),
('name',            ({'en_GB':'full name',        'nl_NL':'naam'},             {'en_GB': 'surname, initials',      'nl_NL': 'achternaam, initielen'})),
('streetAddress',   ({'en_GB':'street address',   'nl_NL':'adres'},            {'en_GB': 'e.g. Rechtstraat 42',    'nl_NL': 'b.v. Rechtstraat 42'})),
('postCode',        ({'en_GB':'post_code',        'nl_NL':'postcode'},         {'en_GB': 'e.g. 1234 XY',           'nl_NL': 'b.v. 1234 XY'})),
('cityAddress',     ({'en_GB':'Town/City',        'nl_NL':'gemeente'},         {'en_GB': 'e.g. Eindhoven',         'nl_NL': 'b.v. Eindhoven'})),
('_locator',         ({'en_GB':'show map',        'nl_NL':'toon kaart'},      {'en_GB': 'show in google maps',    'nl_NL': 'toon met google maps'})),
('phone',           ({'en_GB':'telephone',        'nl_NL':'telefoon'},         {'en_GB': 'e.g. 040-2468135',       'nl_NL': 'b.v. 040-2468135'})),
('emailAddress',    ({'en_GB':'email address(es)','nl_NL':'email addres(sen)'},{'en_GB': 'e.g. 06-24681357',       'nl_NL': 'b.v. 06-24681357'})),
('birthDate',       ({'en_GB':'date__of__birth',  'nl_NL':'geboortedtatum'},   {'en_GB': 'e.g. 15-mrt-1963',       'nl_NL': 'b.v. 15-mrt-1963'})),
('memberSince',     ({'en_GB':'membership date(s)','nl_NL':'lidmaatschapsdatum(s)'},        {'en_GB': 'e.g. 15-okt-2003',       'nl_NL': 'b.v. 15-okt-2003'})),
('instrument',      ({'en_GB':'instrument',       'nl_NL':'instrument'},       {'en_GB': 'e.g. Clarinet',          'nl_NL': 'b.v. Klarinet'})),
('mailGroups', ({'en_GB': 'mailgroup()s', 'nl_NL': 'mailgroups'}, {'en_GB': 'e.g. Clarinet', 'nl_NL': 'b.v. Klarinet'})),
    ])
    formatDict = {'emailAddress': strRef('mailto:'),
                  'phone': strRef('tel:'),
                  'mobile': strRef('tel:'),
                  '_locator': map_url_str,
                  }

    def include_entity(self, member, admin=None):
        # This is a first attempt to make rows_per_entity kind of generic!
        #
        if admin is None:
            admin = self.page.admin
        member._locator = (member.streetAddress + '+' + member.postCode + '+' + member.cityAddress).replace(' ', '+')
        member.current = len(member.memberSince) & 1  # false for departed members
        if member.current:
            self.member_count += 1
            dob, *dod = member.birthDate
            if dob:
                self.age_count += 1
                member.age = date.today() - dob
                self.total_age_days += member.age.days

        return (member.current or admin)

    def put_stats(self):
        yield from h.br
        yield from h| (f"{self.page(en_GB='number of active members', nl_NL='aantal aktieve leden')} = {self.member_count}")
        # no precision argument for format_decimal?! .. just round down
        s_average_age = format_decimal(self.total_age_days / (self.age_count * 365.2425),
                                       format='##0.0',
                                       locale=cherrypy.session.get('language', 'nl_NL'))
        yield from h | f";    {self.page(en_GB='average age', nl_NL='gemiddelde leeftijd')}"
        yield from f" = {s_average_age} {self.page(en_GB='years', nl_NL='jaren')}"
        yield from  h.br

    def __init__(self, page, *path, **kw):
        if not path:
            path = ['current',]
        self.member_count = 0
        self.age_count = 0
        self.total_age_days = 0
        return EntityHelper.__init__(self, page, *path, **kw)
