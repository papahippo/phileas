#!/usr/bin/python3
# -*- encoding: utf8 -*-
from entity.club import MailGroup
import locale
locale.setlocale(locale.LC_ALL, 'nl_NL.utf8')


MailGroup(name = "Conductor", member_re=r'(?!.+)')
MailGroup(name = "Musicians", member_re=r'.+')
MailGroup(name = "Percussion", member_re=r'.*percussi.*|.*slagwerk.*|.*pauken.*|.*timpani.*')
MailGroup(name = "Horn", member_re='.*horn.*')
MailGroup(name = "Flute", member_re='.*flute.*|.*picollo.*')


if __name__=='__main__':
    print ('running %s as main' % __file__)
    # print(MailGroup.keyLookup['name'])
    MailGroup.export()
