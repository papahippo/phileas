from entity.club import Member
Member.purge()

new_Kid_On_The_Block = Member(
    name='Boy, Lost',
    initials='',
    called='new-Kid-On-The-Block',
    streetAddress='',
    postCode='98786 TH',
    cityAddress='',
    phone=('',),
    mobile=(),
    emailAddress=('',),
    altEmailAddress=(),
    birthDate=('',),
    memberSince=('',),
    instrument='percussie',
    mailGroups=[]
)

Donald = Member(
    name='Duck, Donaldus',
    initials='',
    called='Donald',
    streetAddress='De Vijfer 30',
    postCode='1234 AF',
    cityAddress='Eendhoven',
    phone=('06-9876543', '099-1234567'),
    mobile=(),
    emailAddress=('donald.duck42@wetmail.com',),
    altEmailAddress=(),
    birthDate=('12-aug-1952',),
    memberSince=('03-okt-2017',),
    instrument='Flute',
    mailGroups=[]
)

Fresher = Member(
    name='First year',
    initials='',
    called='Fresher',
    streetAddress='',
    postCode='',
    cityAddress='',
    phone=('',),
    mobile=(),
    emailAddress=('',),
    altEmailAddress=(),
    birthDate=('',),
    memberSince=('30-mrt-2020',),
    instrument='',
    mailGroups=[]
)

Conductor = Member(
    name='Conductor, Mr',
    initials='',
    called='Conductor',
    streetAddress='Bokstraat 1',
    postCode='',
    cityAddress='',
    phone=('',),
    mobile=(),
    emailAddress=('',),
    altEmailAddress=(),
    birthDate=('',),
    memberSince=('',),
    instrument='',
    mailGroups=[]
)

Daffie = Member(
    name='Duck, Dafne',
    initials='',
    called='Daffie',
    streetAddress='De Vijfer 30',
    postCode='1234 AD',
    cityAddress='Eendhoven',
    phone=('06-9876543', '099-1234567'),
    mobile=(),
    emailAddress=('second very long email address', 'dafne.duck2@wetmail.com'),
    altEmailAddress=(),
    birthDate=('12-aug-1953',),
    memberSince=('03-jul-2017', '03-jul-2019', '03-sep-2019', '31-mrt-2020'),
    instrument='Clarinet',
    mailGroups=[]
)

Agent_007 = Member(
    name='Bond, James',
    initials='',
    called='Agent 007',
    streetAddress='',
    postCode='',
    cityAddress='',
    phone=('',),
    mobile=(),
    emailAddress=('',),
    altEmailAddress=(),
    birthDate=('',),
    memberSince=('31-mrt-2020',),
    instrument='Beretta',
    mailGroups=[]
)

newby = Member(
    name='Twoshoes, newby',
    initials='',
    called='newby',
    streetAddress='',
    postCode='',
    cityAddress='',
    phone=('',),
    mobile=(),
    emailAddress=('',),
    altEmailAddress=(),
    birthDate=('',),
    memberSince=('',),
    instrument='cello',
    mailGroups=[]
)

fresher = Member(
    name='Hubert Freshman',
    initials='',
    called='fresher',
    streetAddress='',
    postCode='',
    cityAddress='',
    phone=('',),
    mobile=(),
    emailAddress=('',),
    altEmailAddress=(),
    birthDate=('',),
    memberSince=('02-apr-2020',),
    instrument='',
    mailGroups=[]
)

