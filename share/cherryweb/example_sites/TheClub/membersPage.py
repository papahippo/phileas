#!/usr/bin/python3
# -*- encoding: utf8 -*-
from .clubIndexPage import ClubIndexPage, h
import cherrypy
## from share.mew_extra.current import mailgroups
from .memberHelper import MemberHelper
from phileas.cherryweb import url_and_qs, validator



class MembersPage(ClubIndexPage):
##    mailgroups = mailgroups
    lexicon = {
        'yes': {'nl_NL':"ja"},
        'no': {'nl_NL':"nee"},
        'add': {'nl_NL':"voeg toe"},
        'modify': {'nl_NL': "accepteer"},
        'back': {'nl_NL': "terug"},
        'delete': {'nl_NL': "verwijder"},
        'view': {'nl_NL': "toon"},
        'edit': {'nl_NL': "wijzig"},
        'new': {'nl_NL': "nieuw"},
    }

    def admin(self):
        return cherrypy.request.login in ('Admin',)


    def authenticate(self, *paths, **kw):
        with MemberHelper(self, *paths, **kw) as context_:
            # build members' dictionary using 'roepnaam' and postocde as password:
            #members_login_dict = dict(((called, member.postCode)
            #        for called, member in context_.EntityClass.items()))
            members_login_dict = {'club': 'biscuit'}
            print(members_login_dict)
            return validator(members_login_dict)

    # ----------- index page or default (unlnown) page -----------------------
    def body(self, *paths, **kw):
        yield from self.upperBanner(*paths, **kw)
        yield from self.upperText(*paths, **kw)
        with MemberHelper(self, *paths, **kw) as context_:
            yield from self.lowerBanner(context_, **kw)
            yield from self.lowerText(context_, **kw)

    def upperBanner(self, *paths, **kw):
        yield from h.h1(id='upperbanner') \
                   % (f'{self.clubName} - {self(en_GB="Members zone", nl_NL="Ledenzone")}')

    def lowerBanner(self, context_, **kw):
        yield from h.h2(id="lowerbanner") % self(en_GB="Members' zone -  index page",
                                                 nl_NL="Ledenzone - indexpagina")

    def lowerText(self, context_, **kw):
        yield from h.p % self(
            en_GB=(
                "This is the index page of the members' zone. Click one of the followng links to get to the "
                + h.a(href=f'{url_and_qs(lose=1, gain=["list"])}') % "membership list"
                # + " or the " + h.a(href=website.url() + 'music') % "music collection",
                + ". More goodies will be added later as and when needed.",
            ),
            nl_NL=(
                    "Deze indexpagina dient momenteel alleen als tussenstop richting de "
                    + h.a(href=f'{url_and_qs(lose=1, gain=["list"])}') % "ledenlist"
                    + ". Meer splullen zullen t.z.t. toegevoegd worden."
            )
        )

# ------------------------------------------- membership list page ---------------------------------------

    @cherrypy.expose
    def list(self, *paths, **kw):
        yield from self.wrapBody(self.bodyOfList, *paths, **kw)

    def bodyOfList(self, *paths, **kw):
        # correct the (widely advertised so must be supported!) form without path (usually 'current')
        # print('----- paths =', *paths)
        if not paths:
            print("no version specified - current assumed; redirecting...")
            raise cherrypy.HTTPRedirect(cherrypy.url()+'/current')
        yield from self.upperBanner(*paths, **kw)
        yield from self.upperText(*paths, **kw)
        with MemberHelper(self, *paths, **kw) as context_:
            yield from self.lowerBannerOfList(context_, **kw)
            if cherrypy.request.login in ('gill', 'bestuur'):
                yield from h.b % f'{self(en_GB="include ex-members", nl_NL="includeer oud-leden",)}:'
                for choice in ('yes', 'no'):
                    yield from h % ('&nbsp;' * 4) + h.a(
                        href=url_and_qs(*paths, gain=['set_session', 'include_old_members', choice])) % self(choice)
                    if choice == cherrypy.session.get('include_old_members', 'no'):
                        yield from h % f'[={self(en_GB="selected", nl_NL="geselecteerd",)}]'
            yield from context_.list_(**kw)

    def lowerBannerOfList(self, context_, **kw):
        bannerStart =  self(en_GB="Membership list ordered according to field",
                             nl_NL="Ledenlijst gesorteerd op veld",)
        yield from h.h2(id="lowerbanner") % (f"{bannerStart} '{context_.getSortName()}'")

# ------------------------------------------- member view page ---------------------------------------

    @cherrypy.expose
    def view_member(self, *paths, **kw):
        yield from self.wrapBody(self.bodyOfView, *paths, **kw)


    def bodyOfView(self, *paths, **kw):
        yield from self.upperBanner(*paths, **kw)
        yield from self.upperText(*paths, **kw)
        with MemberHelper(self, *paths, **kw) as context_:
            yield from self.lowerBannerOfView(context_, **kw)
            yield from context_.view_(**kw)


    def lowerBannerOfView(self, context_, exception_=None, key_='no-key', **kw):
        yield from h.h2(id="lowerbanner") % self(en_GB=f"Viewing details of member '{key_}'",
                                                 nl_NL=f"Gegevens van lid '{key_}' zijn hieronder getoond", )

# ------------------------------------------- member edit page ---------------------------------------

    @cherrypy.expose
    def edit_member(self, *paths, **kw):
        yield from self.wrapBody(self.bodyOfEdit, *paths, **kw)

    def bodyOfEdit(self, *paths, **kw):
        yield from self.upperBanner(*paths, **kw)
        yield from self.upperText(*paths, **kw)
        with MemberHelper(self, *paths, **kw) as context_:
            yield from self.lowerBannerOfEdit(context_, **kw)
            yield from context_.edit_(**kw)

    def lowerBannerOfEdit(self, context_, exception_=None, key_='no-key', **kw):
        print("edit_banner: key_=", key_)
        new = key_ == '__new__'  # to be improved?
        bannerText = self(
            en_GB=(
                    (new and f"Adding details of new member") or f"Editing details of '{key_}'"
            ),
            nl_NL=(
                    new and f"Toevoegen gegevens nieuw lid" or f"Aanpassen gegevens van '{key_}'"
            )
        )
        yield from h.h2(id="lowerbanner") % bannerText


    @cherrypy.expose
    def validate_member(self, *paths, button_=None, key_=None, **kw):
        """
This is where validate a members details form, or simply recognize a 'cancel' (which can also happen in view mode).
        """
        with MemberHelper(self, *paths, **kw) as context_:
            yield from context_.validate_(button_=button_, key_=key_, **kw)

    def downloadable_list(self):
        if cherrypy.request.login in ('Admin'):
            return self(en_GB="membership list", nl_NL="ledenlijst")
        return False

    @cherrypy.expose
    def download_csv(self, *paths, **kw):
        print ("Ok ook aha!")
        with MemberHelper(self, *paths, **kw) as context_:
            return context_.download_csv(**kw)



_membersPage = MembersPage()
