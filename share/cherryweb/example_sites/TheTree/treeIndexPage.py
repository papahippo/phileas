#!/usr/bin/python3
# -*- encoding: utf8 -*-
import sys, os
from phileas.cherryweb.babelPage import BabelPage, h
from phileas.cherryweb.directoryHelper import DirectoryHelper
from . import CONTENT_BASE
import cherrypy

class TreeIndexPage(BabelPage):
    base_path = CONTENT_BASE

    def admin(self):
        return cherrypy.request.login in ('Admin',)

    @cherrypy.expose
    def list(self, *paths, **kw):
        yield from self.wrapBody(self.BodyOfList, *paths, **kw)

    def BodyOfList(self, *paths, **kw):
        yield from self.upperBanner(*paths, **kw)
        yield from self.upperText(*paths, **kw)

        with DirectoryHelper(self, *paths, **kw) as dir_context_:
            if len(dir_context_):
                yield from h.h2(id="lowerbanner") % self(en_GB="List of subdirectories (which may contain parts",
                                                         nl_NL="Lijst van onderliggende mappen (kunnen ook partijen bevatten)", )
                yield from dir_context_.list_(**kw)


_treeIndexPage = TreeIndexPage()