from phileas.cherryweb import validator, sites_served
import cherrypy
import os

HERE, _ = os.path.split(__file__)
CONTENT_BASE = os.path.join(HERE, 'Content')

from .treeIndexPage import _treeIndexPage


def _treeConfig(session_path=''):
    return {
    '/':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': CONTENT_BASE,
            #'tools.staticdir.index': 'hello.py',
            'tools.sessions.on': True,
            'tools.sessions.storage_class': cherrypy.lib.sessions.FileSession,
            'tools.sessions.storage_path': session_path,
            'tools.sessions.timeout': 10,
            'tools.sessions.locking': 'early'
        },
}
sites_served.append (('/TheTree', _treeIndexPage, _treeConfig))
