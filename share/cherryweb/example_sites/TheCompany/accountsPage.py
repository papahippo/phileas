#!/usr/bin/python3
# -*- encoding: utf8 -*-
from .companyPage import companyName, CompanyPage, h


class AccountsPage(CompanyPage):
    def upperBanner(self,**kw):
        yield from h.h1(id='upperbanner')\
                   % (f'{companyName} - {self(en_GB="Money zone",nl_NL="Geldzone")}')

    def lowerBanner(self, **kw):
        yield from h.h2(id="lowerbanner") % self(en_GB="Members' zone -  index page",
                                                  nl_NL="Ledenzone - indexpagina")

_accountsPage = AccountsPage()
