#!/usr/bin/python3
# -*- encoding: utf8 -*-
from .companyPage import companyName, CompanyPage, h


class AdminPage(CompanyPage):
    admin = True

    def upperBanner(self, **kw):
        return h.h1(id="upperbanner") | f'{companyName}  {self(en_GB="Administration zone", nl_NL="Administratiezone")}'

    def lowerBanner(self, **kw):
        return h.h1(id="lowerbanner") | self(en_GB="Administration Homepage", nl_NL="Homepagina voor Administratiezone")

_adminPage = AdminPage()
