from entity.company import *


DAD = Client(
    number = 1,  # ???
    reference = "",
    name = 'World Trade Channel bv',
    address = ['Piusstraat 130',
        '6467 EH  Kerkrade',
    ]
)

PhNed = Client(
    number = 2,  # ???
    reference = "",
    name = 'Philips Nederland',
    address = ['Gebouw VB-3',
        'Eindhoven',
    ]
)

Beltech = Client(
    number = 3,  # ???
    reference = "IMS-U32502-SFH-P-32475",
    name = 'Beltech',
    address = ['Lodewijkstraat 20',
        '5652 AC Eindhoven',
    ]
)

PhilipsIMS = Client(
    number = 4,  # ???
    reference = "IMS-U32502-SFH-P-32475",
    name = 'Philips Consumer Electronics BV (CD-i)',
    address = ['Postbus 80002 (gebouw SFH-6)',
        '5600 JB Eindhoven',
    ]
)

Carin = Client(
    number = 5,  # ???
    reference = "CED-433291-D -SFH-P-32315",
    name = 'Philips Consumer Electronics BV (Carin)',
    address = ['Postbus 80002 (gebouw SFJ-037)',
        '5600 JB Eindhoven',
    ]
)

QVision = Client(
    number = 6,  # ???
    reference = "",
    name = 'QVision',
    address = ['Waalreseweg 17',
        '5554 HA Valkenswaard',
    ]
)

Polec = Client(
    number = 7,  # ???
    reference = "",
    name = 'Polec Groep CV Eindhoven',
    address = ['Keizersgracht 12',
        '5611 ED Eindhoven',
    ]
)

PhilipsHasselt = Client(
    number = 8,  # ???
    reference = "HAV:886496 00613400 8480 Z 72700",
    name = 'N.V. Philips Industrial Activities',
    address = ['Kempische Steenweg 293',
        'B - 3500 Hasselt',
        'België'
    ]
)

PIMI = Client(
    number = 9,  # ???
    reference = "HAV:886496 00613400 8480 Z 72700",
    name = 'Philips Interactive Media Intenational',
    address = ['188 Tottenham Court Road',
        'London W1P 9LE',
        'England'
    ]
)

PIMC = Client(
    number = 42,  # ???
    reference = "",
    name = 'N.V. Philips Interactive Media Centre',
    address = ['Maastrichterstraat 6',
        'B - 3500 Hasselt',
        'België'
    ]
)

MPDC = Client(
    number = 10,
    reference = "",
    name = 'Motorols Philips Design Centre',
    address = ['Parklaan',
        'Eindhoven',
    ]
)

ASA_Lab = Client(
    number = 11,
    reference = "",
    name = 'Philips Consumer Electronics BV (ASA Lab)',
    address = ['Sound & Vision Account Dept. Dev. TIG (building SWA3)',
        'Postbus 80002',
        '5600 JB  Eindhoven'
    ]
)

MotorolaUK = Client(
    number = 12,
    reference = "",
    name = 'Motorola Limited',
    address = ['Colvilles Road, Kelvin Estate',
        'East Kilbride, Glasgow	G75 0TG',
        'SCOTLAND'
    ]
)

MicrowareUK = Client(
    number = 13,
    reference = "",
    name = 'Microware Systems Limited',
    address = ['Beech Court, 7-33 Summers Road',
        'Burnham, Bucks',
        'SL1 7EP',
        'ENGELAND'
    ],
    btwNumber = 'GB 609 249 333',

)

GerardSmelt = Client(
    number = 14,
    reference = "CSE-823393-D-SBI-07816",
    name = 'Philips Consumer Electronics BV (t.a.v. hr Smelt)',
    address = ['PCS-Accounting Department (building SBP5)',
        'Postbus 218',
        '5600 MD  Eindhoven',
    ],
    btwNumber = 'GB 609 249 333',

)

FNouet = Client(
    number = 15,  # ???
    reference = "",
    name = 'Drs F Nouet',
    address = ['Aquamarijn 15',
        '5629 HC Eindhoven',
    ]
)

PhAlmelo = Client(
    number = 16,  # ???
    reference = "Z211 MFA 081199",
    name = 'Philips Machinefabrieken Nederland BV',
    address = ['Postbus 176',
        '7600 AD Almelo',
    ]
)

VDO_CC = Client(
    number = 17,  # ???
    reference = "VDO-46736-D-32315",
    name = 'VDO Car Communication BV',
    address = ['Glaslaan 2',
        '5616 LW  Eindhoven',
    ]
)

Strobbe = Client(
    number = 18,
    reference = "OS-9 consultancy",
    name = 'Strobbe N.V.',
    address = ['Kasteelstraat 24',
        'B-8870 Izegem',
        'België'
    ]
)


StorkPMT = Client(
    number = 19,  # ???
    reference = "",
    name = 'Stork PMT B.V.',
    address = ['P.O. Box 118',
        '5830 AC Boxmeer',
    ]
)

Nyquist = Client(
    number = 20,  # ???
    reference = "",
    name = 'Nyquist BV',
    address = ['Postbus 7170',
        '5605 JD  Eindhoven',
    ]
)

ASML = Client(
    number = 21,  # ???
    reference = "6030416",
    name = 'ASM Lithography B.V.',
    address = ['Postbus 324',
        '5500 AH  Veldhoven',
    ]
)

Dantech = Client(
    number = 22,  # ???
    reference = "6030416",
    name = 'Dantech bvba',
    address = ['Sacramentspad 1',
        'B-3550 Heusden-Zolder',
    ]
)

PhilipsSemis = Client(
    number = 23,
    reference = "CFT-910696-SAN-88554",
    name = 'Philips Electronics Nederland BV (PSNL)',
    address = ['gebouw VO-p 147',
        'Postbus 80052',
        '5600 KA  Eindhoven',
    ]
)

PhilipsCFT = Client(
    number = 24,
    reference = "CFT-910696-SAN-88554",
    name = 'Philips Electronics Nederland BV (CFT)',
    address = ['gebouw SAQ-p207',
        'Postbus 218',
        '5600 MD  Eindhoven',
    ]
)

MCG = Client(
    number = 25,
    btwNumber = 'GB 609 249 333',
    reference = "Subcontractor agreement 6780",
    name = 'MCG Limited',
    address = ['11 Woodbrook Crescent',
        'Billericay, Essex',
        'CM12 0E',
        'ENGLAND'
    ]
)

MBA = Client(
    number = 26,
    btwNumber = 'GB 524 529 742',
    reference = "Subcontractor agreement 6780",
    name = 'Michael Bailey Associates Ltd',
    address = ['12 Brook House, Chapel Place',
        'Rivington Street',
        'London EC2A 3SJ',
        'ENGLAND'
    ]
)

ProbeIT = Client(
    number = 27,
    btwNumber = 'GB 735 413 348',
    reference = "(NDS contract)",
    name = 'Probe-IT Ltd',
    address = ['Suite 1607',
        '16-19 Southampton Place',
        'London WC1A 2AJ',
        'ENGLAND'
    ]
)

Bariton = Client(
    number = 28,
    reference = "2004 043",
    name = 'Bariton XL BV',
    address = ['Coltbaan 4E',
        '3439 NG  Nieuwegein',
    ]
)

Modis = Client(
    number = 29,
    reference = "schedule B851410 ( payment ref code = E 9205205 )",
    name = 'Modis Europe Ltd',
    address = ['Project House, 110-113 Tottenham Court Road',
        'London W1T 5AE',
        'ENGLAND'
    ]
)

Amaris = Client(
    number = 30,
    reference = "LMY (please advise if more specific reference should be used)",
    name = 'Amaris Consulting Belgium',
    address = ['Chaussée de la Hulpe, 166',
        '1170 Brussels',
        'BELGIUM'
    ]
)

LearnIt = Client(
    number = 31,
    reference = "Python training",
    name = 'Learnit Training B.V.',
    address = ['Antonio Vivaldistraat 52a',
        '1083 HP Amsterdam',
    ]
)

Thesio = Client(
    number = 32,
    reference = "",
    name = 'Thesio Training B.V.',
    address = ['Landgoed Stameren',
        'Amersfoortseweg 1',
        '3951 LA Maarn',
    ]
)

Parcye = Client(
    number = 43,  # ?? was 33 = dup!
    reference = "",
    name = 'Parcye',
    address = ['Lijmbeekstraat 196',
        '5612 NJ  Eindhoven',
    ]
)

Nspyre = Client(
    number = 33,  # ?? dup!
    reference = "",
    name = 'Nspyre B.V.',
    address = ['Herculesplein 24',
        '3584 AA Utrecht ',
    ]
)

Geckotech = Client(
    number = 34,
    reference = """"(conform freelanceovereenkomst dd 11 Juli 2012)""",
    name = 'Geckotech B.V.',
    address = [
        'Ruthardlaan 19',
        '1406RR Bussum',
    ]
)

Oreda = Client(
    number = 35,
    reference = """"(conform “Aannemings contract 201208”)""",
    name = 'Oreda Consulting B.V.',
    address = [
        'Fellenoord 130',
        '5611 ZB Eindhoven',
    ],
    paymentTerms = [
             "Betaling naar bankrekening (zie gegevens boven) binnen 14 dagen wordt op prijs gesteld.",
             "Bij betaling svp factuurnummer vermelden.",
    ]
)

OneAudio = Client(
    number = 36,
    reference = "project 'OneAudio'",
    name = 'R&S Projects',
    address = [
        'Emiel Becquaertlaan 2',
        '2400 Mol',
        'België',
    ],
    btwNumber = "BE 0889 137 038"
)

ComputerFuturesBelgium = Client(
    number = 37,
    reference = "Antwerp Space",
    name = 'Computer Futures',
    address = [
        '11 Rond-point Schuman',
        '1040 Brussels',
        'België',
    ],
    btwNumber = "BE 0892 363 574"
)

YrzEindhoven = Client(
    number = 38,
    reference = "NXP-03",
    name = 'Yrz',
    address = [
        'Turijnstraat 6',
        '5237 ER ‘s-Hertogenbosch',
        'Nederland',
    ],
    btwNumber = "NL1765.37.673.B01",
    paymentTerms = [
             "Betaling naar bankrekening (zie gegevens boven) binnen 60 dagen wordt op prijs gesteld.",
             "Bij betaling svp factuurnummer vermelden.",
    ]
)

AmoriaBond = Client(
    number = 39,
    reference = "J35734",
    name = 'Amoria Bond B.V.',
    address = [
        'Keizersgracht 270',
        '1016 EV Amsterdam',
        'Nederland',
    ],
    btwNumber = "NL8198.86907.B01",
    paymentTerms = [
             "Betaling naar bankrekening (zie gegevens boven) binnen 30 dagen",
             " wordt verwacht i.v.m. verleende korting.",
    ],
)
Acknowledge = Client(
    number = 40,
    reference = "Deelovereenkomst 20160301",
    name = 'Acknowledge Benelux B.V.',
    address = [
        'Postbus 2282',
        '5500BG Veldhoven',
        'Nederland',
    ],
    btwNumber = "NL8094.84.596B03",
    paymentTerms = [
             "Betaling naar bankrekening (zie gegevens boven) binnen 45 dagen",
             " wordt of prijs gesteld.",
    ],
)
Huxley = Client(
    number = 41,
    reference = "Deelovereenkomst 20160301",
    name = 'Huxley IT.',
    address = [
        'De Geelvinck',
        'Singel 540',
        '5500BG Amsterdam',
        'Nederland',
    ],
    btwNumber = "NL8094.84.596B03",
    paymentTerms = [
             "Betaling naar bankrekening (zie gegevens boven) binnen 30 dagen",
             " wordt of prijs gesteld.",
    ],
)
Apollo = Client(
    number = 43,
    reference = "2018HIP/169",
    name = 'Apollo IT Detachering BV',
    address = [
        'Overgoo 15',
        '2266 JZ Leidschendam',
        'Nederland',
    ],
    btwNumber = "NL8546.35.531B01",
    paymentTerms = [
             "Betaling naar bankrekening (zie gegevens boven) binnen 45 dagen"
             " wordt of prijs gesteld.",
    ],
)
KiaDealerAutowijk = Client(
    number = 42,
    reference = "Overeenkomst 166.2017.00000327",
    name = 'Content Autogroep B.V.',
    address = [
        'Pietersbergweg 3-9',
        '5628BS Eindhoven',
        'Nederland',
    ],
    btwNumber = "NL819857427B01",
    paymentTerms = [
             "Dit bedrag mag verrekend worden met de aanschaf van Kia Niro NK-834-S",
             " door onze DGA, de heer Larry Myerscough.",
    ],
)
# print('###', Client.keyLookup)
# print('#####', Company.keyLookup)
