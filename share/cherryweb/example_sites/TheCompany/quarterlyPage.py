#!/usr/bin/python3
# -*- encoding: utf8 -*-
from .companyPage import companyName, CompanyPage
from entity.invoice import InvoiceItem
from entity.outgoing import OutgoingItem
from website42 import LocalContext
import cherrypy
import sys, os

class IncomeHelper(EntityHelper):
    EntityClass = InvoiceItem
    sDefaultSortKey = 'sequenceNumber'

class OutgoingHelper(EntityHelper):
    EntityClass = OutgoingItem
    sDefaultSortKey = 'sequenceNumber'


IMPORT_PATH = os.path.split(__file__)[0] + '/Accounts'


class QuarterlyContext(LocalContext):
    def __init__(self, page, *p, **kw):
        LocalContext.__init__(self, page, *p, import_path=IMPORT_PATH, **kw)

    def __enter__(self):
        LocalContext.__enter__(self)
        # import income
        # self.incomeHelper_ = IncomeHelper(self.page), income)
        print("top of import path'Q' =", sys.path[0]) #
        import outgoings
        self.outgoingHelper_ = OutgoingHelper(self.page_, outgoings)
        return self


class QuarterlyPage(CompanyPage):

    admin = True

    @cherrypy.expose
    def list(self, *paths, **kw):
        with QuarterlyContext(self, *paths, **kw) as context_:
            yield from self.present(self.list_banner, context_.outgoingHelper_.list_, context_, **kw)

    @cherrypy.expose
    def view_outgoingItem(self, *paths, key_='no-key', **kw):
        with QuarterlyContext(self, *paths, **kw) as context_:
            yield from self.present(self.view_banner, context_.outgoingHelper_.view_, context_, key_=key_, **kw)

    @cherrypy.expose
    def edit_outgoingItem(self, *paths, key_='no-key', **kw):
        with QuarterlyContext(self, *paths, **kw) as context_:
            yield from self.present(self.edit_banner, context_.outgoingHelper_.edit_, context_, key_=key_, **kw)

    @cherrypy.expose
    def validate_outgoingItem(self, *paths, button_=None, key_=None, **kw):
        """
This is where validate a members details form, or simply recognize a 'cancel' (which can also happen view mode).
        """
        with QuarterlyContext(self, *paths, **kw) as context_:
            yield from context_.outgoingHelper_.validate_(context_, button_=button_, key_=key_, **kw)

    def upperBanner(self, **kw):
        yield from h.h1(id='upperbanner')\
                   % (f'{companyName} - (supplemental) - {self(en_GB="Members zone",nl_NL="Ledenzone")}')

    def lowerBanner(self, **kw):
        yield from h.h2(id="lowerbanner") % self(en_GB="Members' zone -  index page",
                                                  nl_NL="Ledenzone - indexpagina")

    def lowerText(self, context_, **kw):
        yield from h.p % self(
en_GB=(
"This is the index page of the members' zone. Click one of the followng links to get to the "
+ h.a(href=self.url('list')) % "membership list"
#+ " or the " + h.a(href=website.url() + 'music') % "music collection",
+ ". More goodies will be added later as and when needed.",
),
nl_NL=(
"Deze indexpagina dient momenteel alleen als tussenstop richting de "
+ h.a(href=self.url('list')) % "ledenlist"
+". Meer splullen zullen t.z.t. toegevoegd worden."
)
                #h.p % "2nd paragraph?"
            )
        # yield "more stuff just for testing!"


    def list_banner(self, context_, **kw):
        #bannerStart =  self(en_GB="List of clients ordered according to field",
        #                     nl_NL="Klantenlijst gesorteerd op veld",)
        #yield from h.h2(id="lowerbanner") % (f"{bannerStart} '{context_.outgoingHelper_.getSortName()}'")
        yield from h.h2(id="lowerbanner") %  self(en_GB="List of clients",  nl_NL="Klantenlijst")

    def view_banner(self, context_, exception_=None, key_='no-key', **kw):
        yield from h.h2(id="lowerbanner") % self(en_GB=f"Viewing details of member '{key_}'",
                                                 nl_NL=f"Gegevens van lid '{key_}' zijn hieronder getoond", )

    def edit_banner(self, context_, exception_=None, key_='no-key', **kw):
        new = key_ == '__new__'  # to be improved?
        bannerText = self(
            en_GB=(
                    new and f"Adding details of new expenditure item"
                    or f"Editing details of expenditure item'{key_}'"
            ),
            nl_NL=(
                    new and f"Toevoegen nieuwe uitgave"
                    or f"Aanpassen gegevens van uitgqave '{key_}'"
            )
        )
        yield from h.h2(id="lowerbanner") % bannerText


_quarterlyPage = QuarterlyPage()
