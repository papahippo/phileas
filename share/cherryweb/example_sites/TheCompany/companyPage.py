#!/usr/bin/python3
# -*- encoding: utf8 -*-
from phileas.cherryweb.babelPage import BabelPage, h
import os

companyName = "The Company"


class CompanyPage(BabelPage):
    mountPoint = '/TheCompany'
    _title = companyName
    _upperBanner = companyName
    upperBarColour = 'Green'  # '#6060f0'
    _lowerBanner = "club details"
    lowerBarColour = 'Yellow'
    _synopsis = """dummy synopsis"""
    _detail = """dummy detail - this page is intended to be included, not displayed in its own right!"""
    centreImage = None
    columns = None
    homePage = "/index.py"
    styleSheet =  os.path.join(mountPoint, "theCompany.css")

    def upperBanner(self, *paths, **kw):
        return h.h1(id='upperbanner') % f'{companyName}  - {self(en_GB="Public zone", nl_NL="Openbare zone")}'

    def lowerBanner(self, context_, **kw):
        return h.h1(id='lowerbanner') % self(en_GB="Public Homepage", nl_NL="Homepagina (openbaar)")

    def upperText(self, *paths, **kw):
        yield from h.em % self(
en_GB="WARNING!  This section of the site is really really broken",
nl_NL="WAARSCHUWING!  Dit geldeelte van de site is kei- en keikapot",
        )
        yield from (
            self(
en_GB=(
    h.p %( "These web pages here contain information about 'The (fictitious) company', divided into three zones:"
    + h.ul % (
        h.li % ("The " +  h.a(href=f'{self.mountPoint}/') % " public zone" + " contains some general information. "
             "This is avaliable " + self.languageLink('en_GB', 'in English (in het Engels)')
             + " and ", self.languageLink('nl_NL', 'in Dutch(Nederlands)') +  " - as is all this site "
             "- via these links. "
        )
        + h.li % ("The information in the " + h.a(href=f'{self.mountPoint}/members_zone') % "members zone"
            + " and " + h.a(href=f'{self.mountPoint}/admin_zone') % "adminstration zone" + " is only intended for authorized"
            " members. Hence these zones are protected by passwords."
            " Since this is a fictitious company, there's no harm in telling you that the"
            " user name and passwords of the members zone are respectively 'club' and 'biscuit'!"
                )
    ))
    + "You are welcome to send feedback and enquiries regarding this supplemental site to "
    + h.a(href="mailto:hippostech@gmail.com?Subject=(sent%20via%20phileas/cherrypy%20sample%20page)") % "Larry Myerscough"

),
nl_NL=(
    "Deze webpagina's bevatten informatie over het (fiectieve) bdrijf, verdeeld over drie zones:"
    + h.ul % (
        h.li % ("De " + h.a(href=f'{self.mountPoint}/') % "openbare zone" + " bevat wat algemene informatie."
                " Dit is te bekijken " + self.languageLink('en_GB', 'in het Engels (in English)')
                + " and " + self.languageLink('nl_NL', 'in het Nederlands (in Dutch)')
                + " door middel van deze links. "
                ),
        h.li % ("De inhoud van de " +  h.a(href=f'{self.mountPoint}/members_zone') % "leden zone"
                + " en de " + h.a(href=f'{self.mountPoint}/admin_zone') % "adminstratie zone"
                + " is echter alleen bedoeld voor"
                " geauthoriseerde leden. Daarom zijn deze zones beveiligd met wachtwoorden."
                " Gezien het om een fictief bedrijf gaat, kan het geen kwaad als ik vertel dat de gebruikersnaam"
                  " en wachtwoord voor de ledenzone zijn respectievelijk 'club' and 'biscuit'!"
                )
    )
    + "Met feedback en inlichtingen betreffend deze website kunt u terecht bij "
    + h.a(href="mailto:hippostech@gmail.com?Subject=(sent%20via%20phileas/cherrypy%20sample%20page)") % "Larry Myerscough"
),
                              )
    )

    def lowerText(self, context_, **kw):
        return h.p % ( self(
en_GB=(
    """
    There is not much to see in this 'public zone. Actually, it's not much of a company.
    """
),
nl_NL=(
    """
    Er is helaas weinig te zien in deze 'openbare zone'. Eigenlijk stelt he hele bedrijf weinig voor!
    """
)
                     ),
        )

companyPage_ = CompanyPage()
