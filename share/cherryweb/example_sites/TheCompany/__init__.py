from phileas.cherryweb import validator, sites_served
import cherrypy
import os, tempfile

from .companyIndexPage import _companyIndexPage
from .adminPage import _adminPage
from .accountsPage import _accountsPage
#from .clientsPage import _clientsPage
#from .quarterlyPage import _quarterlyPage

#_companyIndexPage.admin_zone = _clientsPage
#_companyIndexPage.accounts = _accountsPage
#_accountsPage.quarterly = _quarterlyPage

HERE, _ME = os.path.split(__file__)

def company_config(session_path=''):
    return {
    '/':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': HERE,
            'tools.sessions.on': True,
            'tools.sessions.storage_class': cherrypy.lib.sessions.FileSession,
            'tools.sessions.storage_path': session_path,
            'tools.sessions.timeout': 10,
        },
    '/members_zone': {
        'tools.auth_basic.on': True,
        'tools.auth_basic.realm': 'localhost',
        'tools.auth_basic.checkpassword': validator({'club': 'biscuit'}),
        'tools.auth_basic.accept_charset': 'UTF-8',
    },
    '/accounts': {
        'tools.auth_basic.on': True,
        'tools.auth_basic.realm': 'localhost',
        'tools.auth_basic.checkpassword': validator({'Admin': 'istrator'}),
        'tools.auth_basic.accept_charset': 'UTF-8',
    },
}
sites_served.append (('/TheCompany', _companyIndexPage, company_config))