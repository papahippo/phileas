#!/usr/bin/python3
# -*- encoding: utf8 -*-
from .companyPage import companyName, CompanyPage
from entity.company import Client
from website42 import LocalContext
import cherrypy
import os

IMPORT_PATH = os.path.split(__file__)[0] + '/current'

class ClientHelper(EntityHelper):
    EntityClass = Client

    #fieldDisplay = OrderedDict([ ... to be filled in!
    formatDict = {'emailAddress': strRef('mailto:'),
                  'phone': strRef('tel:'),
                  'mobile': strRef('tel:'),
                  '_locator': map_url_str,
                  }

    def include_entity(self, client):
        # This is a first attempt to make rows_per_entity kind of generic!
        #
        #member._locator = (member.streetAddress + '+' + member.postCode + '+' + member.cityAddress).replace(' ', '+')
        #member.current = len(member.memberSince) & 1  # false for departed members
        #return (member.current or self.page.admin)
        client.current = client.number > 0
        return True

class ClientContext(LocalContext):
    def __init__(self, *p, **kw):
        LocalContext.__init__(self, *p, import_path=IMPORT_PATH, **kw)

    def __enter(self, *p):
        LocalContext.__enter__(self)
        import clients
        self.clientHelper_ = ClientHelper(self.page_, clients)
        return self


class ClientsPage(CompanyPage):

    lexicon = {
        'Add': {'nl_NL':"Voeg toe"},
        'Modify': {'nl_NL': "Accepteer"},
        'Back': {'nl_NL': "Terug"},
        'Delete': {'nl_NL': "Verwijder"},
        'View': {'nl_NL': "Toon"},
        'Edit': {'nl_NL': "Wijzig"},
        'new': {'nl_NL': "nieuw"},
    }

    admin = False

    @cherrypy.expose
    def list(self, *paths, **kw):
        with ClientContext(*paths, **kw) as context_:
            yield from self.present(self.list_banner, context_.clientHelper_.list_, context_, **kw)

    @cherrypy.expose
    def view_client(self, *paths, key_='no-key', **kw):
        with ClientContext(*paths, **kw) as context_:
            yield from self.present(self.view_banner, context_.clientHelper_.view_, context_, key_=key_, **kw)

    @cherrypy.expose
    def edit_client(self, *paths, key_='no-key', **kw):
        with ClientContext(*paths) as context_:
            yield from self.present(self.edit_banner, context_.clientHelper_.edit_, context_, key_=key_, **kw)

    @cherrypy.expose
    def validate_client(self, *paths, button_=None, key_=None, **kw):
        """
This is where validate a members details form, or simply recognize a 'cancel' (which can also happen view mode).
        """
        with ClientContext(*paths) as context_:
            yield from context_.clientHelper_.validate_(context_, button_=button_, key_=key_, **kw)

    def upperBanner(self,**kw):
        yield from h.h1(id='upperbanner')\
                   % (f'{companyName} - (supplemental) - {self(en_GB="Members zone",nl_NL="Ledenzone")}')

    def lowerBanner(self,**kw):
        yield from h.h2(id="lowerbanner") % self(en_GB="Members' zone -  index page",
                                                  nl_NL="Ledenzone - indexpagina")

    def lowerText(self, context_, **kw):
        yield from h.p % self(
en_GB=(
"This is the index page of the members' zone. Click one of the followng links to get to the "
+ h.a(href=self.url('list')) % "membership list"
#+ " or the " + h.a(href=website.url() + 'music') % "music collection",
+ ". More goodies will be added later as and when needed.",
),
nl_NL=(
"Deze indexpagina dient momenteel alleen als tussenstop richting de "
+ h.a(href=self.url('list')) % "ledenlist"
+". Meer splullen zullen t.z.t. toegevoegd worden."
)
                #h.p % "2nd paragraph?"
            )
        # yield "more stuff just for testing!"


    def list_banner(self, context_, **kw):
        #bannerStart =  self(en_GB="List of clients ordered according to field",
        #                     nl_NL="Klantenlijst gesorteerd op veld",)
        #yield from h.h2(id="lowerbanner") % (f"{bannerStart} '{context_.clientHelper_.getSortName()}'")
        yield from   h.h2(id="lowerbanner") %  self(en_GB="List of clients", nl_NL="Klantenlijst")

    def view_banner(self, context_, exception_=None, key_='no-key', **kw):
        yield from h.h2(id="lowerbanner") % self(en_GB=f"Viewing details of member '{key_}'",
                                                 nl_NL=f"Gegevens van lid '{key_}' zijn hieronder getoond", )

    def edit_banner(self, context_, exception_=None, key_='no-key', **kw):
        new = key_ == '__new__'  # to be improved?
        bannerText = self(
            en_GB=(
                    new and f"Adding details of new member" or f"Editing details of '{key_}'"
            ),
            nl_NL=(
                    new and f"Toevoegen gegevens nieuw lid" or f"Aanpassen gegevens van '{key_}'"
            )
        )
        yield from h.h2(id="lowerbanner") % bannerText


_clientsPage = ClientsPage()
