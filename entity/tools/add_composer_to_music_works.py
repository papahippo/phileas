from entity.music import MusicWork
import sys, os, csv, re

def main():
    csvfile = open(sys.argv[1], 'r')
    with  csvfile as file_:
        csv_reader = csv.reader(file_, delimiter=';')
        attr_names = csv_reader.__next__()
        previous = None
        i_updated = 0
        MusicWork.autoAttach = False
        creArranger = re.compile(r'(.+)[\s-]arr\.(.*)')
        for i_line, value_row in enumerate(csv_reader):
            # print((attr_names, value_row))
            # print(dict(zip(attr_names, value_row)))
            kw = dict(zip(attr_names, value_row))
            new_part_member = MusicWork(**kw)
            serial = new_part_member.serial
            try:
                corr_existing_member = MusicWork.keyLookup['serial'][serial]
            except KeyError:
                print(f"no corresponding existingrecord for serial {serial} (line no. ca. {2 + i_line})")
                continue
            if new_part_member.composers:
                match = creArranger.match(new_part_member.composers[0])
                if match:
                    before, after  = match.groups()
                    if after:
                        new_part_member.composers = before,
                        new_part_member.arrangers = after,
                    else:
                        new_part_member.composers = ()
                        new_part_member.arrangers = before,
                corr_existing_member.composers = new_part_member.composers
                corr_existing_member.arrangers = new_part_member.arrangers
                i_updated += 1
        print(f"updated {i_updated} MusicWork instances.")
        out_name = '/home/gill/gill_on_tera/MEW_Archive/musicWorks1.py'
        MusicWork.export(out_name)
        print(f"exported updated MusicWork instances to {out_name}.")

if __name__ == "__main__":
    sys.path.insert(0, '/home/gill/gill_on_tera/MEW_Archive')
    import musicWorks
    # print (MusicWork.keyLookup['serial'])
    main()
