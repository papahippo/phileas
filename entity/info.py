#!/usr/bin/python
# -*- encoding: utf8 -*-
from entity import *
# from collections.abc import Callable
import sys, os


class Info(Entity):
    """
    This is provional implementation
    """
    keyFields = ('name',)

    def __init__(self,
                 name: str = '',
                 synopsis = '', # : Callable[[int], None]=lambda f:None,
                 detail= '', # : Callable[[int], None]=lambda f:None,
                 ):
        if os.path.sep in name:
            dir_name =  name.split(os.path.sep)[-2]
            name = as_python_name(dir_name)
        Entity.__init__(self,
                        name=name,
                        synopsis=synopsis,
                        detail=detail,
                        )

if __name__ == "__main__":
    def synopsis(lingo):
        yield dict(en_GB='silly synosis', nl_NL='stomme synopsis',)[lingo]

    def detail(lingo):
        yield dict(en_GB='"disappointing lack of detail"', nl_NL='teleurstellend gebrek aan detail',)[lingo]
    info = Info(name=__file__,
                synopsis=synopsis,
                detail=detail
                )
    print(info)
    #print(Info.keyLookup)
    print( [(name_, info_) for name_, info_  in Info.keyLookup['name'].items()])
    print( list(Info.keyLookup['name'].values())[0].detail)


