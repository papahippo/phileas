#!/usr/bin/python
# -*- encoding: utf8 -*-
from entity import *
from entity.club import MailGroup
import sys, os


class MusicWork(Entity):
    keyFields = ('title', 'serial')

    scratchSerial = 99000

    def __init__(self,
                title: str = '',
                serial:int=0,
                 digital:bool=False,
                path:str='',
                composers: List[str] = [],
                arrangers: List[str] = [],
                ):

        if not serial:
            MusicWork.scratchSerial += 1
            serial = MusicWork.scratchSerial
        Entity.__init__(self,
                        title=title,
                        serial=serial,
                        digital=digital,
                        composers=composers,
                        arrangers=arrangers,
                        path=path,
                        )


class MusicPart(Entity):
    keyFields = ('name',)  # minimal implementation 'for now'

    def __init__(self,
                 name: str = '',
                 path: str = '',
                 mailGroup:str='',
                 token: str = '',
                 ):
        if not name:
            name = as_python_name(path.split('/')[-1])
        Entity.__init__(self,
                        name=name,
                        path=path,
                        mailGroup=mailGroup,
                        token=token,
                        )


class Abc_tune(Entity):
    keyFields = ('location', 'titles')

    def __init__(self,
                 location:str='',
                 titles:List[str]=[],
                 filename:str='',
                 xref:int=1,
                 composers:List[str]=[],
                 sources:List[str]=[],
                 ):
        if (not location) and filename:
            location = f"{os.path.splitext(filename)[0]}_X{xref}"
        Entity.__init__(self,
                        location=location,
                        titles=titles,
                        sources=sources,
                        filename=filename,
                        xref=xref,
                        composers=composers,
                        )
if __name__ == "__main__":
    abc_tune = Abc_tune(filename='test.abc', titles=('imaginary tune', 'first part'), composers=['nonone'])
    print(abc_tune)

