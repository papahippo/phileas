#!/usr/bin/python
# -*- encoding: utf8 -*-
from entity import *
import sys, os


class Directory(Entity):
    """
    This rather degenerate looking entity is designed to facilitate browsing through directories containing
    entities of a different kind, togetehr with subdirectories.
    """
    keyFields = ('name', 'dir_name')  # minimal implementation 'for now'

    def __init__(self,
                 name: str = '',
                 dir_name: str = '',
                 ):
        if not name:
            name = as_python_name(dir_name)
        Entity.__init__(self,
                        name=name,
                        dir_name=dir_name,
                        )
if __name__ == "__main__":
    directory = Directory(dir_name='some_dir')
    print(directory)

