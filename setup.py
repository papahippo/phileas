import sys, os, pathlib
from setuptools import setup, find_packages

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

SHARE = pathlib.Path('share/cherryweb')
share_dirs = [str(SHARE / Dir) for Dir in [
                    'css',  # to be added to!
]]

share_dirs_here = [str(HERE / share_dir) for share_dir in share_dirs]

data_files = [(share_dir, [os.path.join(share_dir, each_file) for each_file in os.listdir(share_dir_here) if not each_file.startswith('__')])
            for share_dir, share_dir_here in zip(share_dirs, share_dirs_here)]


print(data_files)
# This call to setup() does all the work
setup(
    name='phileas',
    version='0.9.82',
    packages=find_packages(),  # doesn't recurse... ['phileas', 'entity'],
    install_requires=['cherrypy', 'babel'],
    data_files=data_files,
    url='https://gitlab.com/papahippo/phileas',
    license='',
    author='Larry Myerscough',
    author_email='hippostech@gmail.com',
    description='Python with Html In-Line Embeddewd At Source (somewhat contrived acronym)'
)

