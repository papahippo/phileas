import webbrowser, tempfile, time, os, locale

def gloss(language=None, **texts):
    try:
        return texts[language or locale.getlocale()[0]]
    except KeyError:
        return texts['en_GB']

def browse(content_generator, seconds=None):
    # htmlFile = tempfile.NamedTemporaryFile(mode='w', prefix='phileas_browse_', suffix='.html', delete=False)
    html_filename = os.path.join(tempfile.gettempdir(), 'phileas_browse.html')
    with  open(html_filename, 'w') as hf:
        for fragment in content_generator:
            hf.write(fragment)
    webbrowser.open(f"file://{hf.name}", False, True)
    if seconds:
        time.sleep(seconds)
        os.remove(hf.name)

if __name__ == '__main__':
    from phileas import html5 as h
    import sys

    def Info():
        yield from h.h1 % 'header category 1'
        yield from h.em % 'emphatic' + h.br
        yield from h % 'normal' + h.br
    progName = sys.argv.pop(0)
    seconds = sys.argv and float(sys.argv.pop(0))
    browse(Info(), seconds)