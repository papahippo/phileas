"""
The definitions in this file correspond to the rules of HTML5
"""

from .html_ import HTML
from .element import Element

PresAttrs = {'style': 1, 'fill': 1} # ? to be added!

class SVG_Element(Element):
    attr_dict = {}

class SVG(HTML):

    class A(SVG_Element):
        pass

    class Title(SVG_Element):
        pass  # not sure this is correct

    class AltGraph(SVG_Element):
        attr_dict = ({'x': 1, 'y': 1, 'dx': 1, 'dy': 1},) # etc.!

    class Circle(SVG_Element):
        attr_dict = ({'cx': 1, 'cy': 1, 'r': 1}, PresAttrs) # etc.!

    class G(SVG_Element): # = group
        attr_dict = ({'id': 1, 'fill': 1, 'opacity': 1,}, PresAttrs) # etc.!

    class Line(SVG_Element):
        attr_dict = ({'x1': 1, 'y1': 1, 'x2': 1, 'y2': 1, }, PresAttrs)

    class Polyline(SVG_Element):
        attr_dict = ({'points': 1,}, PresAttrs)

    class Rect(SVG_Element):
        attr_dict = ({'x': 1, 'y': 1, 'rx': 1, 'ry': 1, 'width': 1, 'height': 1},
                     PresAttrs) # etc.!

    class Text(SVG_Element):
        attr_dict = ({'x': 1, 'y': 1, 'dx': 1, 'dy': 1, 'width': 1, 'height': 1},
                     PresAttrs) # etc.!


