from . import html5 as h, svg as s, audio, browse

def my_svg():
    yield from s.polyline(points="0,40 40,40 40,80 80,80 80,120 120,120 120,160",
                                style="fill:white;stroke:red;stroke-width:4")
def blurb():
    yield from h.html | (
            (""" This primitive web-page was produced using 'phileas' (and python of course!).
            """ + (h.em | 'aha!')
             )
        )
    line = (h.em | "this") + "and that " + (h.em | "and the other")
    line += "and more!"
    yield from (h.html | line)
    p = h.p
    ul = h.ul()
    for i in range(3):
        ul += h.li | "item %d" %i
    p |= ul
    yield from p
    yield from h.html % (h.svg(height=180, width=500) % my_svg())
    # following shows up but only plays if a valid '/tmp/t.mp3' is present!
    yield from (h.audio(controls=True) |  audio.source(src='t.mp3', type="audio/mpeg"))


if __name__ == '__main__':
    browse(blurb())
    # print ([b for b in blurb()])