#!/usr/bin/python3
# -*- encoding: utf8 -*-
from __future__ import print_function
import sys, os, time
from phileas import html5 as h
from entity import EntityError
from entity.club import Member
import cgi
import cgitb
#cgitb.enable()

from urllib.parse import urlparse, parse_qs

# N.B. This page is obsolecsent; It needs some refactoring to fit in with other changes in package 'phileas'.
# My company administration stuff still depends on it, but since my company is no more, refactoring is not urgent.
# The assocatedsample site 'TheCompany' is temporarily(?) outof service.

class Page:
    topDir = os.path.split(__file__)[0]
    styleSheet = "/.style/phileas.css"
    metaDict = {'http-equiv': "content-type", 'content': "text/html; charset=utf-8"}
    _title = '(untitled)'

    def __init__(self, localIndex=None):
        self.errOutput = []
        self.resolveData()

    def resolveData(self):
        """
'resolveData'is just a 'hook' at this level. Furthermore, its role has been largely taken over
by 'validate'. It is still uses by some of my company administration stuff; to be investigated!
"""
        pass

    def title(self):
        yield self._title

    def head(self):
        yield from h.meta(**self.metaDict) + (
            (self.styleSheet and
             h.link(type="text/css", rel="stylesheet", href=self.styleSheet)),
            h.title  % self.title()
        )

    def write(self, s):
        """ We provide our own 'write' function so that we can handle
        our own standard error output.
        """
        self.errOutput.append(str(s))

    def body(self):
        #return "abcdé".encode('ascii','xmlcharrefreplace').decode('ascii')
        print(
            "(gratuitous 'error' output) current directory is:",
            os.getcwd(),
            file=sys.stderr
        )
        yield from ('default body of content... abcdéf'
                + h.br
                + h.br
                + h.p % 'end of content'
                )

    def html(self):
        yield from (
            h.head % self.head()
            + h.body % (self.body() + h.pre % self.errOutput)
        )

    def validate(self, **kw):
        """
'validate' interprets the 'keywords' (actually cgi-parameters) passed to the page. It returns
True if this page is be presented. Alternatively it may cause some other page to be presented
and return False.
        """
        self.kw = kw  # stub / base class version
        return True  # =>  # go ahead an prsent this page.

    def present(self):
        sys.stderr = self
        print("Content-type: text/html;charset=UTF-8\n\n")  # the blank line really matters!
        print((''.join(self.html()))  ) # .encode('ascii','xmlcharrefreplace').decode('ascii'))

    def asFileName(self, path):
        if path[0] != '/':
            return path
        return self.topDir + path

    def asUrl(self, fileName):
        if fileName[0] != '/':
            return fileName
        return fileName[len(self.topDir):]

    def main(self):
        """
    This main function is designed to allow pages to be viewed either in a browser context or in a
    conventional programming context. Its detection implicitly assumes that the Apache server is in use....
    which isn't true - not even for my own use! TO BE REVIEWED.
        """
        uri = os.environ.get('REQUEST_URI')
        if uri:
            self.script_name = os.environ['SCRIPT_NAME']
            o = urlparse(uri)
            path = os.environ['DOCUMENT_ROOT'] + o.path  # geturl()
            if not os.path.isdir(path):
                path = os.path.split(path)[0]
            os.chdir(path)
            kw = parse_qs(o.query)
        else:
            self.script_name = sys.argv[0]
            kw = {}
            for p in sys.argv[1:]:
                key_, vals_ = p.split('=')
                kw[key_] = vals_.split(',')

        if self.validate(**kw):
            self.present()


if __name__ == "__main__":
    Page().main()
