import cherrypy
from phileas import html5 as h
import sys, urllib
from phileas.cherryweb import BabelPage


class DefaultIndexPage(BabelPage):

    _title = "phileas/website example page"
    styleSheet = '/css/default.css'
    name = "website page"

    def body(self, *paths, **kw):
        yield from self.languageBlurb(*paths)
        yield from h.h1(align='center') % "'Phileas Cherry' web server - default index page"
        yield from self( en_GB="This is the default top level page when the server is started by...",
                         nl_NL="Deze 'default' pagina wordt getoond wanneer de server is opgestart met...",
                         ) + h.br*2
        yield from   (h.em % "python3 -m phileas.cherryweb") + h.br*2
        yield from ("... but this behaviour can be (and for a live site, invariably should be!) overruled by making"
                    " changes to ... " + h.br*2
        + (h.em % '.../phileas/cherryweb/sites/__init__.py') + h.br*2)

        yield from ("After making such changes you can still show this page by staring the server as..."
                    + h.br*2
                    + h.em % 'python3 -m phileas.cherryweb.sites._default' + h.br*2)
        for mountPoint in cherrypy.tree.apps.keys():
            yield from h.a(href=mountPoint) % (mountPoint[1:] if mountPoint else "(top level)") + h.br


_defaultIndexPage = DefaultIndexPage()
