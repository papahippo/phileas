from cherrypy.lib import auth_basic, sessions
from .defaultIndexPage import DefaultIndexPage, _defaultIndexPage
from phileas.cherryweb import sites_served
import os
HERE, _ = os.path.split(__file__)
DEFAULT_BASE = os.path.normpath(os.path.join(HERE, '..', '..', '..', 'share', 'cherryweb'))
print(f"DEFAULT_BASE={DEFAULT_BASE}")

def _defaultConfig(session_path=''):
    return {
    '/':
        {
            'tools.sessions.on': True,
            'tools.sessions.storage_class': sessions.FileSession,
            'tools.sessions.storage_path': session_path,
            'tools.sessions.timeout': 10,
        },
    '/css':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': DEFAULT_BASE + "/css",
        },
    }
sites_served.append(('/', _defaultIndexPage, _defaultConfig))

