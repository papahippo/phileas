#!/usr/bin/python3
# -*- encoding: utf8 -*-
"""
This is work in progress. I want to make the editing listing viewing etc. of an entity more generic....
But I don't want to impose a page-style at the generic level...
Nor do I want to enforce a one entity class per page restriction.
I'm startinmg by cannibalizing a copy of TheClub/membersPage....
"""
import os
from collections import OrderedDict
from entity.directory import Directory
from .entityHelper import EntityHelper, h
import cherrypy

#def link_to_subdir(s):


class DirectoryHelper(EntityHelper):
    EntityClass = Directory
    _moduleForImport = 'directories'
    sDefaultSortKey = 'dir_name'
    exclude = '_.'  # this is provisional. The ',' has to do with restyling the abc_music site!

    def buildModuleForImport(self):
        print(f"'{self.moduleForImport}'  module not (yet) present: let's build it! ...")
        for fn in os.listdir(self.import_path):
            if fn[0] in self.exclude:
                continue  # to avoid showing e.g. '__pycache__'
            fqn = os.path.join(self.import_path, fn)
            if not os.path.isdir(fqn):
                continue
            Directory(dir_name=fn)
        return self.exportBuiltModule()

    fieldDisplay = OrderedDict([
        # python name       # heading for glossing                              # field entry tip for glossing
        # ('name',       ({'en_GB': 'name', 'nl_NL': 'naam'},)*2),
        ('dir_name',   ({'en_GB': 'directory name', 'nl_NL': 'mapnaam'},) * 2),
    ])

    #formatDict = {
    #    'directoryname': link_to_subdir,
    #              }

    def yield_link_fields(self, entity):
        # print("yielding directory link fields?!")
        yield from (h.a(href=os.path.join(cherrypy.url(), entity.dir_name))
                         % entity.dir_name)

    def yield_input_fields(self, exception_=None, name_='no-name', **kw):
        pass  # not sure what belongs here!
