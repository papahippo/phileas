#!/usr/bin/env python3
import cherrypy
from cherrypy.process.plugins import DropPrivileges
import sys, os, subprocess, shutil, tempfile
from . import sites_served

def main():
    print(f"running'main' in {__file__}")
    localDir = os.path.abspath(os.path.dirname(__file__))

    # need local ip address;
    # this approach works for my dev system and real garage but is not generically robust:
    #
    local_ip_address = subprocess.check_output(['hostname', '-s', '-I']).decode('utf-8').split(' ')[0]
    server_prog_name = sys.argv.pop(0) if sys.argv else '[unknown]'
    config_key = sys.argv.pop(0) if sys.argv else 'dev'
    uid = sys.argv.pop(0) if sys.argv else None
    gid = sys.argv.pop(0) if sys.argv else uid
    if config_key == 'dev':
        global_config = {
            'server.socket_host': local_ip_address,
            'server.socket_port': 8080,
            'server.thread_pool': 10,
        }
    elif config_key == 'test':
        global_config = {
            'server.socket_host': "127.0.0.1",
            'server.socket_port': 8443,
            'environment': 'production',
            'log.screen': True,
            'server.ssl_module': 'builtin',
            'server.ssl_certificate': os.path.join(localDir, 'server.crt'),
            'server.ssl_private_key': os.path.join(localDir, 'server.key'),
            'server.thread_pool': 10,
        }
    else:
        # assume live mmode; confi_key gives certificate name
        print(f"{server_prog_name} will swill user certification of '{config_key}'",
              file=sys.stderr)
        certDir = os.path.join('/etc/letsencrypt/live', config_key)
        config_key = 'live'
        global_config = {
            'tools.sessions.locking': 'early',
            'server.socket_host': local_ip_address,  # adjust to your requirements!
            'server.socket_port': 443,
            'environment': 'production',
            'log.screen': True,
            'server.ssl_module': 'builtin',
            'server.ssl_certificate': os.path.join(certDir, 'fullchain.pem'),
            'server.ssl_private_key': os.path.join(certDir, 'privkey.pem'),
            'server.thread_pool': 10,
        }

    print(f"{server_prog_name} will start website server with configuration '{config_key}'",
          file=sys.stderr)
    if (uid):
        print(f"... and will drop privilegs to user '{uid}', group {gid}")

    if uid:
        DropPrivileges(cherrypy.engine, uid=uid, gid=gid).subscribe()

    for mountPoint, page_, config_ in sites_served:
            session_path = tempfile.mkdtemp(suffix=f'_{mountPoint[1:]}_cherrypy_sessions')
            if uid:
                shutil.chown(session_path, uid, gid)
            cherrypy.tree.mount(page_, mountPoint, config_(session_path=session_path))
            page_.mountPoint = mountPoint
            print(f"mounted '{mountPoint}'", file=sys.stderr)

    cherrypy.config.update(global_config)
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    main()
