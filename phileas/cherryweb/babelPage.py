import cherrypy
import phileas
print('phileas.__file__ =', phileas.__file__)
from phileas import html5 as h
from phileas.cherryweb import url_and_qs
import sys, os, tempfile, urllib

HERE = os.path.split(__file__)[0]

class BabelPage:

    base_path = HERE

    styleSheet = '/css/default.css'
    name = "website page"
    _title = "phileas/website example page"
    _upperBanner = _title
    upperBarColour = 'SlateBlue'  # '#6060f0'
    _lowerBanner = "[default lower banner]"
    lowerBarColour = 'Orange'
    mountPoint = '/'
    defaultLanguage = 'en_GB'
    lexicon = {}

    def downloadable_list(self):
        return False  # catch-all default; arguably belnogs elsewhere

    def metas_etc(self):
        yield from h.meta(http_equiv="content-type", content= "text/html; charset=utf-8")

    def head(self):
        yield from self.metas_etc()
        if self.styleSheet:
            yield from h.link(type="text/css", rel="stylesheet", href=self.styleSheet)
            yield from h.title % self.title()

    def title(self):
        return self._title


    def write(self, s):
        """ We provide our own 'write' function so that we can handle
        our own standard error output.
        """
        self.errOutput.append(str(s))

    @cherrypy.expose
    def index(self, *paths, **kw):
        yield from self.wrapBody(self.body, *paths, **kw)


    def wrapBody(self, bodyFunc, *paths, **kw):
        self.errOutput = []  # class level initialization is surely dangerous!
        sys.stderr = self
        yield from h.head % self.head()
        yield from h.body % bodyFunc(*paths, **kw)
        yield from (h.pre % '\n'.join(self.errOutput))

    def __call__(self, key='', **kw):
        """
This member function comes under the heading of "naughty python"! It replaces the global function 'gloss' so that
the default language can vary between pages. Rather then having to code 'self.self(en_GB=...' etc. for all the many language
sensitive elements of a page, I wanted to be able to code the shorter 'self(en_GB=...' etc.
        """
        if not kw:
            kw = self.lexicon.get(key, {})

        try:
            lingo = cherrypy.session.setdefault('language', self.defaultLanguage)
        except AttributeError:
            lingo = self.defaultLanguage  # get here e.g. if sessions haven't [yet] been properly configured
        return kw.get(lingo, key)

    def title(self):
        return self._title

    def upperBanner(self, *paths, **kw):
        yield from (h.h1(id='upperbanner') %self._upperBanner)

    def lowerBanner(self, context_, **kw):
        yield from (h.h1(id='lowerbanner') % self._lowerBanner)

    def body(self, *paths, **kw):
        yield from self.upperBanner(*paths, **kw)
        yield from self.upperText(*paths, **kw)
        yield from self.lowerBanner(None, **kw)
        yield from self.lowerText(None, **kw)

    @cherrypy.expose
    def set_session(self, var_, value_, *paths, **kw):
        # ugly workaround to allow pasing of value as argument.
        if value_[0]=='_':
            value_ = kw[value_[1:]]
        print('var_', var_, 'value_', value_, cherrypy.session.id)
        cherrypy.session[var_] = urllib.parse.unquote(value_)
        url = url_and_qs(*paths, lose=3, gain=[], qs=kw)
        print("redirecting to", url)
        raise cherrypy.HTTPRedirect(url)

    def languageLink(self, language_code, language_text, *paths):
        return h.a(href=url_and_qs(*paths,
                                   gain=['set_session', 'language', language_code])) % language_text

    def languageBlurb(self, *paths):
        yield from h.p % (h.em % self(
            en_GB=(
                "Klik hier voor " + self.languageLink('nl_NL', 'Nederlands', *paths)
            ),
            nl_NL=(
                "Click hier voor " + self.languageLink('en_GB', 'English', *paths)
            )
        ))

    def upperText(self, *paths, **kw):
        yield from self.languageBlurb(*paths)
        yield from h.p % self(
            en_GB=(
                "This demo is an example of how the 'phileas' package can be used"
                " in combination with the 'website' package to serve a primitive web-site. "
            ),
            nl_NL=(
                "Deze demo-page is een voorbeeld van hoe men de 'phileas' package kan gebruiken"
                "in combinatie met de 'website' package om een eenvoudige web-site tot stand te brengen."
            ),
        )

        yield from h.p % self(
            en_GB=(
                "This content of this section (between the two 'banner bars')"
                " is determined by the 'upperText' member function of the page class."
            ),
            nl_NL=(
                "Deze inhoud wordt bepaald door de 'upperText' member function van de page class."
            ),
        )

    def lowerText(self, context_, **kw):
        yield from h.p % self(
                en_GB=("""
This content is determined by the 'lowerText' member function of the page class.
                """),
                nl_NL=("""
Deze inhoud wordt bepaald door de 'lowerText' member function van de page class.
                """),
            )

_babelPage = BabelPage()
