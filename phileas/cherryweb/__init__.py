import cherrypy
sites_served = []

def validator(dick):
    """'validate_password' is a utility function for use when defining password-protected
        sections of a website in the configuration file.
        "..share/cherryweb/example_sites/TheClub/__init__.py" shows how to use this.  """

    def validate_password(realm, username, password):
        if password and  dick.get(username) == password:
            cherrypy.session['username'] = username
            return True
        return False
    return validate_password

def url_and_qs(*paths, lose=0, gain=[], qs=True, fragment=''):
    """'url_and_qs' is a utility function for making relative 'jumps' and 'jump backs'
    within sites. I can't find the genuine(cherrrpy or other) idiom for what I'm trying to do.
    (... so perhaps I shouldn't be trying to do this at all?!)
    It returns  a modified copy of the current url (including query string).
    'lose' is how may elements to remove from the url path starting with the element reprersenting the
    current server path (i.e. before any path elements passed by cherrypy as '*p'.
    'gain' is a list of path elements to add to the url at the same point.
    WARNING: This function was introduced late on with the intention of replacing more cumbersome
    methods involving session variables. It currently does not always work according to the above
     description.
    """
    # print(f"paths={paths}, lose={lose}, gain={gain}, qs={qs}")
    if qs is True:
        qs = cherrypy.request.query_string
    elif isinstance(qs, dict):
        qs = '&'.join([f"{k_}={v_}" for k_, v_ in qs.items()])
    parts = cherrypy.url().split('/')
    l = len(parts)
    p = len(paths) + (lose==0)
    new_parts = parts[0:l-p-lose] + gain + parts[l-p:]
    # print(f"url_and_qs: p={p} l={l} {parts} -> {new_parts} (qs={qs})")
    # following 'belt and braces' code worhty of refinement!
    if qs and qs[0]!='?':
        qs = '?'+qs
    return '/'.join(new_parts)+qs+fragment

from .babelPage import BabelPage, _babelPage
from .default_site import *
