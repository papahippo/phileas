#!/usr/bin/python3
# -*- encoding: utf8 -*-
"""
This is work in progress. I want to make the editing listing viewing etc. of an entity more generic....
But I don't want to impose a page-style at the generic level...
Nor do I want to enforce a one entity class per page restriction.
I'm startinmg by cannibalizing a copy of TheClub/membersPage....
"""
import os, importlib
from collections import OrderedDict
from entity import (Entity, EntityError)
import cherrypy
from cherrypy.lib import static
from phileas import html5 as h
from phileas.cherryweb import url_and_qs


def strRef(text):
    def fn(stringList):
        return  h.br.join([h.a(href=text + string) % string for string in stringList])
    return fn

def comma_sep(s):
    if isinstance(s, (list, tuple)):
        return ', '.join([str(s1) for s1 in s])
    return s

def as_html_text(s):
    return s.replace('_', '&nbsp;')

def map_url_str(s):
    return h.a(href='https://www.google.com/maps/place/'+s) % '?'

def YesNo(flag):
    return 'Yes' if flag else 'No'

def one_per_row(s):
    if isinstance(s, (int, float)):
        s = str(s)
    if isinstance(s, (list, tuple)):
        return h.br.join([str(s1) for s1 in s])
    return s

def decap(s):
    return s[:1].lower()+s[1:]


class EntityHelper:
    EntityClass = Entity
    _moduleForImport = 'entities'

    fieldDisplay = {}  # => construct a makeshift field display dictionary at runtime.
    formatDict = {}  # use default formatting in all cases

    sDefaultSortKey = 'name'

    def __init__(self, page, *paths, prefix='', **kw):
        self.page = page
        self.url_path = os.path.join(page.mountPoint, *paths)
        self.import_path = os.path.join(page.base_path, *paths)
        self.paths_ = paths
        self.moduleForImport = prefix + self._moduleForImport
        self.entity_name = decap(self.EntityClass.__qualname__)

    def __enter__(self):
        self.EntityClass.purge()
        for retrying in (False, True):
            self.moduleForImportFilename = os.path.join(self.import_path, self.moduleForImport) + '.py'
            try:
                self.imported_module = importlib.machinery.SourceFileLoader(self.moduleForImport,
                                                                            self.moduleForImportFilename).load_module()
                setattr(self, self.moduleForImport, self.imported_module)
                break
            except FileNotFoundError:
                print('FileNotFoundError')
                if not retrying:
                    answer = self.buildModuleForImport()
                    if answer is True:
                        continue  # retry after successful build of module
                    elif answer is None:
                        self.imported_module = None
                        return None  # don't care
                    else:   # in practive presumably False
                        print(f"can't build module '{self.moduleForImport}' ...")
                        raise
        return self

    def __exit__(self, type_, value_, traceback_):
        # self.helper.EntityClass.purge()
        self.imported_module = None

    def buildModuleForImport(self):
        # in general we can't just rebuild the index at will...
        # but let's assume an empty file is acceptable in genreal:
        return None

    def __len__(self):
        return len(self.EntityClass)

    def getSortKey(self):
        return cherrypy.session.get('sortby', self.sDefaultSortKey)

    def getSortName(self):
        # print (self, 'self.fieldDisplay', self.fieldDisplay.keys())
        return self.page(**self.fieldDisplay[self.getSortKey()][0])

    def validate_(self, button_=None, key_=None,  **kw):
        """
This is where we validate a members details form, or simply recognize a 'cancel' (which can also happen view mode).
        """
        print(f"key_='{key_}'")
        if not key_:
            key_ = kw[self.EntityClass.keyFields[0]]
        if button_ != self.page("back"):
            print(f"key_='{key_}'")
            print(f"button_='{button_}'")
            ##inst = (key_ != '__new__') and self.EntityClass.by_key(key_)
            try:
                inst = self.EntityClass.by_key(key_)
            except KeyError:
                inst = None
            if inst and button_ != self.page("Add"):
                try:
                    print("detaching!")
                    inst.detach()
                except KeyError:
                    print(f"failed to detach {self.entity_name} with key '{key_}'")
                    pass
            if button_ in (self.page("add"), self.page("modify") ):
                try:
                    # Retrieve the fields and values and use these to create a new or replacement instance.
                    self.new_instance = self.EntityClass(**kw)
                    print('created new instance', self.new_instance)
                except EntityError as ee:
                    print("Exception while validating!", ee)
                    if button_ == self.page("modify"):
                        inst.attach()
                    # Hmm.. possibly better to return a boolean her and let real page take initiative.
                    #
                    yield from getattr(self.page, 'edit_' + self.entity_name)(*self.paths_, exception_=ee, key_=key_, **kw)
                    return
            print(f"exporting updated '{self.moduleForImportFilename}' file (python module)")
            self.EntityClass.export(self.moduleForImportFilename)
            csv_file_name = os.path.splitext(self.moduleForImportFilename)[0] + '.csv'
            print(f"exporting updated '{csv_file_name}' (e.g. for import to spreadsheet)")
            self.export_csv(self.EntityClass, csv_file_name)
        # print("successful change (or just cancel): redirecting from %s ..." %website.url())
        raise cherrypy.HTTPRedirect( url_and_qs( *self.paths_, lose=1, gain=['list',], qs=''))

    def exportBuiltModule(self):
        print('exporting...')
        self.EntityClass.export(f'{self.import_path}/{self.moduleForImport}.py')
        print('exported, now purging...')
        self.EntityClass.purge()
        print('purged!')
        return True  # = 'yes I have rebuilt the module'

    def export_csv(self, cls, out_file):
        with open(out_file, 'w') as file_:
            print(';'.join([self.page(**heading)
                  for attr_name, (heading, tip_text) in self.fieldDisplay.items() if not attr_name.startswith('_')]),
                  file=file_)
            print (*[';'.join([str(getattr(member, attr_name))
                 for attr_name, (heading, tip_text) in self.fieldDisplay.items() if not attr_name.startswith('_')])
                    for name, member in sorted(self.EntityClass.keyLookup[self.getSortKey()].items())
                    if self.include_entity(member, admin=False)],
                   sep='\n', file=file_)


    def list_(self, **kw):
        # print('!!!', self.EntityClass.keyLookup)
        self.row_count = -1
        sk = self.getSortKey()
        yield from h.table(id="members") % [(self.rows_per_entity(entity))  # also need to change id=entities?!
                                            for name, entity in
                                            ## sorted(({sk: {}})[sk].items())]
                                            sorted((self.EntityClass.keyLookup or {sk: {}})[sk].items())]
        yield from self.put_stats()

        if self.page.downloadable_list():
            yield from h.br
            yield from h.p % (
                self.page(en_GB="""
You may download this list in 'csv' form, by clicking on the link below. If you then import it
into a spreadsheet program, be sure to specify semi-colon (';') as separator. Do not rely on such output
unnecesarily; the on-line version is of course more likely to be up-to-date!
                    """,nl_NL="""
Je mag het lijst downloaden is csv formaat door op de link hieronder te klikken. Bij het eventueel importeren
dat dit lijst in een spreadsheet programma, moet je punt-comma (';') selecteren als scheidingsteken.
Gebruik zulke output met beleid; de on-line versie blijft beter up-to-date!
                        """)
                + h.br*2
                +  (h.a(href=url_and_qs(self.paths_, lose=1, gain=['download_csv',], qs=''))
                    % self.page(en_GB=f"download {self.page.downloadable_list()} in 'csv' format",
                                nl_NL=f"download {self.page.downloadable_list()} in 'csv' formaat"))
            )

    def download_csv(self, **kw):
        csv_file_name = f"{self.moduleForImport}.csv"
        # prevent backdoor access:
        if not self.page.downloadable_list():
            raise cherrypy.HTTPError(401, f"you are not permitted to download '{csv_file_name}'!")
        print("import_path = ", self.import_path, ", downloading", csv_file_name)
        return static.serve_file(f"{self.import_path}/{csv_file_name}", 'application/x-csv',
                                 'attachment', csv_file_name, debug=True)

    def include_entity(self, entity, admin=None):
        entity.current = True
        return True  # anything goes for base class!

    def put_stats(self):
        yield ''

    def rows_per_entity(self, entity):
        admin = self.page.admin()
        if not self.include_entity(entity):
            return
        self.row_count += 1
        if self.row_count % 15 == 0:
            yield from h.tr(id='tableguide') % (
                h.th % (admin and (h.a(href=url_and_qs(*self.paths_, lose=1,
                                                 gain=[f"edit_{self.entity_name}"],
                                                      qs='?key_=__new__'))   % self.page('new'))
            or '...')
                + [h.th % ((attr_name in self.EntityClass.keyFields and
                          h.a(href=url_and_qs(*self.paths_,
                                              gain=['set_session', 'sortby', attr_name]))
                          or h) % as_html_text(self.page(**heading)))
                 for attr_name, (heading, tip_text) in self.fieldDisplay.items()
                 ])
        yield from h.tr % (
            h.td % self.yield_link_fields(entity)
            + [h.td % ((entity.current and h or h.dEL) %
                       (self.formatDict.get(attr_name, one_per_row)(getattr(entity, attr_name))))
             for attr_name, (heading, tip_text) in self.fieldDisplay.items()]
        )

    def yield_link_fields(self, entity):
        admin = self.page.admin()
        yield from (h.a(id=f'{getattr(entity, entity.keyFields[0])}',
                         href=(url_and_qs(*self.paths_, lose=1,
                                        gain=[f"{'edit' if admin else 'view'}_{self.entity_name}"],
                                        qs= "?key_=" + getattr(entity, entity.keyFields[0]))))
                     % (admin and self.page("edit") or self.page("view")))

    def yield_input_fields(self, exception_=None, key_='no-key', **kw):
        admin = self.page.admin()
        inst = (key_ != '__new__') and not exception_ and self.EntityClass.by_key(key_)
        for (attr_name, (displayed_name, placeholder)) in self.fieldDisplay.items():
            if attr_name.startswith('_'):
                continue
            yield from (h.label(For=f'{attr_name}') % as_html_text(self.page(**displayed_name)) +
            '<input type = "text" title="testing!" STYLE="color:%s;" name = "%s" value="%s"><br />\n'
                % ('#000000', attr_name, comma_sep(inst and getattr(inst, attr_name) or kw.get(attr_name, ''))))

        yield from self.yield_softkey('back', 'green')
        if admin:
            if key_ == '__new__':
                yield from self.yield_softkey('add', 'orange')
            else:
                yield from self.yield_softkey('modify', 'orange')
                yield from self.yield_softkey('delete', 'red')
        if exception_:
            yield from  (h.br * 2)
            yield from h.a(STYLE="color:#ff0000;") % f"error! {exception_}"

    def yield_softkey(self, text, background_colour):
        yield from h.input(type ="submit", name="button_", STYLE=f"background-color:{background_colour}",
                           value=self.page(text)) % ''

    def edit_(self, exception_=None, key_='no-key', **kw):
        #print('!!adjusted path=', self.page.url(f'validate_{self.entity_name}', *self.paths_),
        #      'key_=', key_, key_ != '__new__', key_ == '__new__', type(key_))
        action = url_and_qs(*self.paths_, lose=1,
                                            gain=[f'validate_{self.entity_name}',],
                                            qs=f'?key_={key_}')
        print(f"EntityHelper.edit_: action={action}")
        yield from h.form(action=action, method='get') % self.yield_input_fields(exception_, key_, **kw)

    view_ = edit_
