#!/usr/bin/python3
# -*- encoding: utf8 -*-
"""
This is transitional stuff. I'm phasing out my (not so?) bright idea of 'Info.py' files
"""
from .entityHelper import EntityHelper
from entity.info import Info


class InfoHelper(EntityHelper):
    EntityClass = Info
    _moduleForImport = 'info'
