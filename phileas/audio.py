"""
The definitions in this file correspond to the rules of HTML5
"""

from .html_ import HTML
from .element import Element

class Audio_Element(Element):
    attr_dict = {}

class Audio(HTML):

    class Source(Audio_Element):
        attr_dict = ({'src': 1, 'type': 1},) # etc.?

# unfinished, I think?
