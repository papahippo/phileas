#!/usr/bin/python3
# this script isn't strictly necessary;  'python -m phileas.cherryweb' is equivalent...
# ... but this approach seems to work better in Pycharm debugger.

from phileas.cherryweb.default_site.__main__ import main
main()
